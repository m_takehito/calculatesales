package jp.alhinc.matsuura_takehito.売上集計;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {

	public static void main (String[] args) {
		File branchLst = new File(args[0], "branch.lst");

		//【エラー：ファイルが存在しない場合】
		if (!branchLst.exists()) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}

		HashMap<String, String> branchMap = new HashMap<>();//支店コード,支店名
		HashMap<String, Long> totalMap = new HashMap<>();//支店コード,合計金額
		BufferedReader br = null;
		//支店定義ファイルの読み込み
		try {
			br = new BufferedReader(new FileReader(branchLst));
			String line;
			while ((line = br.readLine()) != null) {

				//読み込んだデータを分ける
				String data[] = line.split(",");
				//【エラー：支店定義ファイルのフォーマットが指定と異なる場合】
				if (data.length != 2 || !data[0].matches("^[0-9]{3}$") || data[1].isEmpty()) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//読み込んだデータを分ける

				String code = data[0];
				String name = data[1];

				//支店コードと支店名の保持
				branchMap.put(code, name);
				totalMap.put(code, 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();

				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}

			try {
				FilenameFilter filter = new FilenameFilter() {
					public boolean accept(File dir, String filename) {
						//拡張子と8桁の数字でフィルタリング
						return filename.matches("[0-9]{8}.rcd") && new File(dir, filename).isFile();
					}
				};

				File[] rcdFile = new File(args[0]).listFiles(filter);
				for (int i = 0; i < rcdFile.length; i++) {

					//ファイル名を取得
					String fileName = rcdFile[i].getName();
					int number = Integer.parseInt(fileName.substring(0, 8));

					//【エラー：売上ファイルが連番になっていない場合】
					if (i != 0) {
						String oldFileName = rcdFile[i - 1].getName();
						int oldnumber = Integer.parseInt(oldFileName.substring(0, 8));
						if (number - oldnumber != 1) {
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
					}
					//フィルタをかけたファイルのデータを読み込む
					br = new BufferedReader(new FileReader(rcdFile[i]));

					List<String> salesList = new ArrayList<String>();

					//データを追加
					String slLine;
					while ((slLine = br.readLine()) != null) {
						salesList.add(slLine);
					}

					//【エラー：売上ファイルの中身が3行以上ある場合】
					if (salesList.size() != 2) {
						System.out.println(fileName + "のフォーマットが不正です");
						return;
					}

					String sales = salesList.get(1);
					Long longSales = Long.parseLong(sales);
					String branchCode = salesList.get(0);

					//【エラー：支店に該当がない場合】
					if (!branchMap.containsKey(branchCode)) {
						System.out.println(fileName + "の支店コードが不正です");
						return;
					}

					Long totalSales = totalMap.get(branchCode) + longSales;

					//【エラー：合計金額が10桁を超えた時】
					int digit = String.valueOf(totalSales).length();
					if (digit > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					totalMap.put(branchCode, totalSales);
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}

		//ファイルへの出力
		File outputFile = new File(args[0], "branch.out");
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
			for (HashMap.Entry<String, Long> entry : totalMap.entrySet()) {
				bw.write(entry.getKey() + "," + branchMap.get(entry.getKey()) + "," + entry.getValue());
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}